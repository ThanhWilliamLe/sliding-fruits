﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Collections;

public static class ObjectUtils
{
    public static List<KeyValuePair<T, string>> GetObjectsOfType<T>(bool scene = true, bool assets = true) where T : Object
    {
        List<KeyValuePair<T, string>> l = new List<KeyValuePair<T, string>>();
        if (scene)
        {
            foreach (T t0 in Object.FindObjectsOfType<T>())
            {
                l.Add(new KeyValuePair<T, string>(t0, "scene"));
            }
        }
#if UNITY_EDITOR
        if (assets)
        {
            foreach (T t0 in Resources.FindObjectsOfTypeAll<T>())
            {
                l.Add(new KeyValuePair<T, string>(t0, "asset"));
            }
        }
#endif
        return l;
    }

    public static bool HasColor(Component c)
    {
        if (c is SpriteRenderer) return true;
        if (c is Graphic) return true;
        if (c is Text) return true;
        return false;
    }

    public static Color GetColor(Component c)
    {
        if (c is SpriteRenderer) return ((SpriteRenderer)c).color;
        if (c is Graphic) return ((Graphic)c).color;
        if (c is Text) return ((Text)c).color;
        return Color.white;
    }

    public static void SetColor(Component c, Color c0)
    {
        if (c is SpriteRenderer) ((SpriteRenderer)c).color = c0;
        else if (c is Graphic) ((Graphic)c).color = c0;
        else if (c is Text) ((Text)c).color = c0;
    }

    public static bool SetSprite(Component c, Sprite s)
    {
        if (c is SpriteRenderer)
        {
            ((SpriteRenderer)c).sprite = s;
            return true;
        }
        if (c is Image)
        {
            ((Image)c).sprite = s;
            return true;
        }
        return false;
    }

    public static bool SetMonoEnabled<T>(GameObject o, bool b) where T : MonoBehaviour
    {
        T comp = o.GetComponent<T>();
        if (comp != null)
        {
            comp.enabled = b;
            return true;
        }
        return false;
    }

    public static void ClearChildrens(Transform t, bool immediate = false)
    {
        for (int i = t.childCount - 1; i >= 0; i--)
        {
            if (immediate) Object.DestroyImmediate(t.GetChild(i).gameObject);
            else Object.Destroy(t.GetChild(i).gameObject);
        }
    }

    public static IEnumerator DisableObjectIn(GameObject o, float t)
    {
        yield return new WaitForSeconds(t);
        o.SetActive(false);
    }

    public static Color MergeHSV(Color cFrom, Color cTo, bool keepH, bool keepS, bool keepV)
    {
        float h0, s0, v0;
        float h1, s1, v1;
        Color.RGBToHSV(cFrom, out h0, out s0, out v0);
        Color.RGBToHSV(cTo, out h1, out s1, out v1);
        return Color.HSVToRGB(keepH ? h1 : h0, keepS ? s1 : s0, keepV ? v1 : v0);
    }

    public static void MarkDirty(Object o)
    {
#if UNITY_EDITOR
        if (Application.isEditor) UnityEditor.EditorUtility.SetDirty(o);
#endif
    }
}