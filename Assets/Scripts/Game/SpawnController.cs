﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour
{
    [Header("Spawn objects")]
    public Transform fallingsGroup;
    public GameObject fallingPrefab;
    public GameObject shadowPrefab;

    public Transform roadFruitsGroup;
    public GameObject roadFruitPrefab;

    [Header("Spawn config")]
    public Vector2 spawnCount = new Vector2(1, 3);
    public Vector2 spawnInterval = new Vector2(0.5f, 0.9f);
    public Vector2 spawnFlyTime = new Vector2(2f, 2.5f);

    [Header("Get harder")]
    public float harderInterval = 2f;
    public float spawnCountMult = 1.15f;
    public float spawnIntervalMult = 0.975f;
    public float spawnFlyTimeMult = 0.98f;

    [Header("Fruit change")]
    public float fruitInterval = 15;
    public float fruitAhead = 3;
    public float fruitEachDelay = 0.15f;

    [Header("Boom revive")]
    public float boomDelay = 0.05f;

    Queue<GameObject> fallingPool = new Queue<GameObject>();
    Queue<GameObject> shadowPool = new Queue<GameObject>();
    float timePassed;
    float lastHarderTime;
    float lastFruitTime;

    public Player Player
    {
        get { return GameManager.CurrentPlayer; }
    }

    public bool CanBehave
    {
        get { return GameManager.CurrentGame.IsPlaying(); }
    }

    public float SpawnExtend
    {
        get { return GameManager.CurrentGame.roadWidth; }
    }

    private void Update()
    {
        if (CanBehave)
        {
            timePassed += Time.deltaTime;
            float interval = Random.Range(spawnInterval.x, spawnInterval.y);
            interval /= Mathf.Pow(Player.speed / Player.startSpeed, 0.3f);

            if (timePassed > interval)
            {
                timePassed = 0;
                DoSpawn();
            }

            lastHarderTime += Time.deltaTime;
            if (lastHarderTime > harderInterval)
            {
                lastHarderTime -= harderInterval;
                spawnCount *= spawnCountMult;
                spawnInterval *= spawnIntervalMult;
                spawnFlyTime *= spawnFlyTimeMult;
            }

            lastFruitTime += Time.deltaTime;
            if (lastFruitTime > fruitInterval)
            {
                DoSpawnFruit();
                lastFruitTime -= fruitInterval;
            }
        }
    }

    void DoSpawnFruit()
    {
        int failsafe = 0;
        FruitModelData fmd = Player.fmd;
        while (fmd == Player.fmd && failsafe++ < 100)
        {
            fmd = FruitModelResources.Instance.GetRandom();
        }
        if (fmd != null && fmd != Player.fmd)
        {
            Vector3 totalSize = Vector3.Scale(fmd.scale, fmd.colliderSize);
            totalSize.y = 0;
            float size = totalSize.magnitude;

            int spawnCount = Mathf.FloorToInt(SpawnExtend * 2f / size / 2f);
            for (int i = 0; i < spawnCount; i++)
            {
                float xPos = SpawnExtend * (i - (spawnCount - 1) / 2f) / spawnCount * 2f;
                float zPos = Player.transform.position.z + Player.speed * fruitAhead;

                GameObject go = Instantiate(roadFruitPrefab, roadFruitsGroup);
                go.transform.position = new Vector3(xPos, 0, zPos);

                RoadFruit rf = go.GetComponent<RoadFruit>();
                rf.SetModelData(fmd);
                rf.Spawn(fruitEachDelay * i);
            }
        }
    }

    void DoSpawn()
    {
        float pspeed = Player.speed;
        float pmspeed = Player.maxSpeed;
        float pacce = GameManager.CurrentGame.acceleration;
        float maxAcceTime = Mathf.Max(0, (pmspeed - pspeed) / pacce);

        int count = Mathf.CeilToInt(Random.Range(spawnCount.x, spawnCount.y));
        for (int i = 0; i < count; i++)
        {
            float timeTilGround = Random.Range(spawnFlyTime.x, spawnFlyTime.y);
            float acceTime = Mathf.Min(timeTilGround, maxAcceTime);
            float spawnHeight = -GameManager.CurrentGame.gravity / 2 * timeTilGround * timeTilGround;

            float xPos = RandomXPos(i, count - 1);
            float yPos = spawnHeight;
            float zPos = Player.transform.position.z +
                timeTilGround * (pspeed + acceTime * pacce) + 
                (timeTilGround - acceTime) * pmspeed;

            //if (zPos > SpawnMaxZ) continue;

            GameObject falling = UnpoolFalling();
            falling.transform.position = new Vector3(xPos, yPos, zPos);
            FallingObject fall = falling.GetComponent<FallingObject>();
            fall.OnSpawn(SpawnEntries.Instance.GetRandomFallingAt(zPos).fmd, timeTilGround - Time.fixedDeltaTime);

            GameObject fallingShadow = UnpoolShadow();
            FallingShadow shadow = fallingShadow.GetComponent<FallingShadow>();
            shadow.OnSpawn(fall);
        }
    }

    float RandomXPos(int i, int imax)
    {
        float length = SpawnExtend * 2;
        float unitLength = length / (imax + 1);
        return -SpawnExtend + Random.Range(unitLength * i, unitLength * (i + 1));
    }

    GameObject UnpoolFalling()
    {
        if (fallingPool.Count == 0)
        {
            GameObject go0 = Instantiate(fallingPrefab, fallingsGroup);
            go0.GetComponent<FallingObject>().SetFMD(FallingModelResources.Instance.GetRandom());
            fallingPool.Enqueue(go0);
        }
        GameObject go = fallingPool.Dequeue();
        return go;
    }

    GameObject UnpoolShadow()
    {
        if (shadowPool.Count == 0)
        {
            shadowPool.Enqueue(Instantiate(shadowPrefab, fallingsGroup));
        }
        GameObject go = shadowPool.Dequeue();

        return go;
    }

    public void PoolFalling(GameObject falling)
    {
        fallingPool.Enqueue(falling);
    }

    public void PoolShadow(GameObject shadow)
    {
        shadowPool.Enqueue(shadow);
    }

    public void BoomFallings(System.Action callback)
    {
        StartCoroutine(DoBoomFallings(callback));
    }

    IEnumerator DoBoomFallings(System.Action callback)
    {
        int counter = 0;
        FallingObject[] fos = fallingsGroup.GetComponentsInChildren<FallingObject>();
        for (int i = 0; i < fos.Length; i++)
        {
            if (fos[i].CanBehave)
            {
                StartCoroutine(BoomAFalling(fos[i], counter * boomDelay));
                counter++;
            }
        }
        if (callback != null)
        {
            yield return new WaitForSecondsRealtime(counter * boomDelay);
            callback.Invoke();
        }
    }

    IEnumerator BoomAFalling(FallingObject fo, float delay)
    {
        yield return new WaitForSecondsRealtime(delay);
        GameManager.CurrentGame.fo.BoomKnifeAt(fo.transform.position);
        fo.transform.position = new Vector3(-100, -100, -100);
        if (fo.fs != null) fo.fs.transform.position = new Vector3(-100, -100, -100);
    }
}
