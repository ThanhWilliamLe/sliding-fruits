﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    [Header("Objects")]
    public SpawnController sc;
    public InputController ic;
    public UIController uc;
    public FloorObject fo;
    public CameraObject co;
    public Player player;
    public SideLineObject[] slos;

    [Header("Configs")]
    public float roadWidth = 10;
    public float roadOuterWidth = 12;
    public float gravity = -20;
    public float acceleration = 0.1f;

    [Header("States")]
    public GameState gameState;
    public bool canHaveSecondChance = true;

    [Header("Buffs")]
    public float recoverBoostTime;
    public float infiniteJuiceTime;

    private float _score;
    public float Score
    {
        get { return _score; }
        set
        {
            _score = Mathf.Max(value, 0);
        }
    }
    private float _juiceLeft;
    public float JuiceLeft
    {
        get { return _juiceLeft; }
        private set
        {
            _juiceLeft = Mathf.Max(value, 0);
        }
    }

    private void Awake()
    {
        GameManager.CurrentGame = this;
        gameState = GameState.Init;
    }

    private void OnDestroy()
    {
        Time.timeScale = 1;
        if (GameManager.CurrentGame == this) GameManager.CurrentGame = null;
    }

    private void Reset()
    {
        sc = GetComponent<SpawnController>();
        ic = GetComponent<InputController>();
        uc = FindObjectOfType<UIController>();
        fo = FindObjectOfType<FloorObject>();
        co = FindObjectOfType<CameraObject>();
        player = FindObjectOfType<Player>();
        slos = FindObjectsOfType<SideLineObject>();
    }

    private void Start()
    {
        InitGame();
        StartGame();
    }

    [ContextMenu("Init game")]
    public void InitGame()
    {
        player.SetModelData(FruitModelResources.Instance.defaultData, true);
        AdaptFMD();
        co.FindCameraFOV();
        fo.FillCamera();
        foreach (SideLineObject slo in slos) slo.MoveToPlace();
#if UNITY_EDITOR
        if (Application.isEditor && !Application.isPlaying)
        {
            UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
        }
#endif
    }

    public void PlayerChangedFruit()
    {
        if (JuiceLeft < player.fmd.juiceContent.maxJuice)
        {
            JuiceLeft = Mathf.Min(JuiceLeft + player.fmd.juiceContent.juiceBurst,
                player.fmd.juiceContent.maxJuice);
        }
        AdaptFMD();
        player.dieable = false;
        uc.Flash(player.fmd.uiColor, () => { player.dieable = true; });
    }

    public void AdaptFMD()
    {
        uc.ApplyFMD(player.fmd);
        co.ApplyFMD(player.fmd);
        fo.ApplyFMD(player.fmd);
        foreach (SideLineObject slo in slos) slo.ApplyFMD(player.fmd);
    }

    public void StartGame()
    {
        gameState = GameState.Play;
        co.StartTracking();
        fo.StartTracking();
        player.StartMoving();
        Score = 0;
        JuiceLeft = player.fmd.juiceContent.startJuice;
        canHaveSecondChance = true;
    }

    public void PauseGame()
    {
        if (gameState == GameState.Play)
        {
            gameState = GameState.Pause;
            Time.timeScale = 0;
            uc.OnPauseGame();
        }
    }

    public void UnpauseGame()
    {
        if (gameState == GameState.Pause)
        {
            gameState = GameState.Play;
            Time.timeScale = 1;
            uc.OnUnpauseGame();
        }
    }

    public void GoStore()
    {
        if (gameState == GameState.Play)
        {
            gameState = GameState.Pause;
            Time.timeScale = 0;
            uc.ShowJuiceUI();
        }
    }

    public void ExitStore()
    {
        if (gameState == GameState.Pause)
        {
            gameState = GameState.Play;
            Time.timeScale = 1;
            uc.HideJuiceUI();
        }
    }

    [ContextMenu("Find road width")]
    public void FindRoadWidth()
    {
        Camera cam = Camera.main;
        Vector3 vp = cam.WorldToViewportPoint(player.transform.position);

        Ray r0 = cam.ViewportPointToRay(new Vector3(0, vp.y));
        Ray r1 = cam.ViewportPointToRay(new Vector3(1, vp.y));

        Vector3 v0 = r0.origin + r0.direction * (player.transform.position.y - r0.origin.y / r0.direction.y);
        Vector3 v1 = r1.origin + r1.direction * (player.transform.position.y - r1.origin.y / r1.direction.y);

        float totalWidth = Mathf.Abs(v1.x - v0.x);
        roadOuterWidth = totalWidth / 2;

        if (player.mr != null)
        {
            float size = (Matrix4x4.Scale(new Vector3(1, 0, 1)) * player.mr.bounds.size).magnitude;
            totalWidth -= size * 1.25f;
        }
        roadWidth = totalWidth / 2;
    }

    private void Update()
    {
        if (IsPlaying())
        {
            if (recoverBoostTime > 0) recoverBoostTime = Mathf.Max(0, recoverBoostTime - Time.deltaTime);
            if (infiniteJuiceTime > 0) infiniteJuiceTime = Mathf.Max(0, infiniteJuiceTime - Time.deltaTime);

            player.speed = Mathf.Clamp(player.speed + Time.deltaTime * acceleration, player.startSpeed, player.maxSpeed);
            Score += Time.deltaTime;

            if (JuiceLeft < player.fmd.juiceContent.maxJuice)
            {
                float recover = Time.deltaTime * player.fmd.juiceContent.juiceRecover;
                if (recoverBoostTime > 0) recover *= 2;

                JuiceLeft = Mathf.Min(JuiceLeft + recover, player.fmd.juiceContent.maxJuice);
            }
        }
    }

    public bool IsPlaying()
    {
        return gameState == GameState.Play;
    }

    public bool IsGaming()
    {
        return gameState == GameState.Play || gameState == GameState.Pause;
    }

    public void OnPlayerDied()
    {
        gameState = GameState.End;
        uc.OnGameOver();
    }

    internal void GiveSecondChance()
    {
        gameState = GameState.Pause;
        sc.BoomFallings(() =>
        {
            gameState = GameState.Play;
            player.Revive();
            JuiceLeft = Mathf.Max(JuiceLeft, player.fmd.juiceContent.startJuice);
        });
        canHaveSecondChance = false;
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void AddScoreToCoin()
    {
        GameManager.Instance.Coin += ScoreToCoin(Score);
        Score = 0;
    }

    public int ScoreToCoin(float sc)
    {
        return Mathf.FloorToInt(sc / 10);
    }

    public void OnTap()
    {
        if (JuiceLeft >= 1f || infiniteJuiceTime > 0)
        {
            player.SwitchDirection();
            GameManager.CurrentGame.uc.gameUI.Tapped();
            if (infiniteJuiceTime <= 0) JuiceLeft -= 1f;
            //if (JuiceLeft <= player.fmd.juiceContent.juiceLowWarning) uc.gameUI.ShowJuiceGuide();
        }
    }

    public void BoughtItem(JuiceMarketEntry jme)
    {
        bool canHappen = true;
        if (jme.videoType == JuiceMarketEntry.VideoType.None)
        {
            if (jme.cost <= GameManager.Instance.Coin)
            {
                GameManager.Instance.Coin -= jme.cost;
                canHappen = true;
            }
            else canHappen = false;
        }

        if (canHappen)
        {
            if (jme.type == JuiceMarketEntry.Type.BurstJuice) JuiceLeft += jme.amount;
            else if (jme.type == JuiceMarketEntry.Type.RecoverJuice) recoverBoostTime += jme.amount;
            else if (jme.type == JuiceMarketEntry.Type.InfiniteJuice) infiniteJuiceTime += jme.amount;
        }
    }
}

public enum GameState
{
    Init, Play, Pause, End
}
