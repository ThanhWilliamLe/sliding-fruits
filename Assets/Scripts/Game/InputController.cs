﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InputController : MonoBehaviour
{
    public void OnTap(PointerEventData eventData)
    {
        GameManager.CurrentGame.OnTap();
    }
}
