﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(fileName = "Falling model resources", menuName = "Game/Falling model resources")]
public class FallingModelResources : ScriptableObject
{
    private static FallingModelResources instance;
    public static FallingModelResources Instance
    {
        get
        {
            if (instance == null) instance = Resources.Load<FallingModelResources>("Falling model resources");
            return instance;
        }
    }

    public FallingModelData defaultData;
    public List<FallingModelData> modelDatas = new List<FallingModelData>();

    public FallingModelData this[int i]
    {
        get
        {
            if (i >= 0 && i < modelDatas.Count) return modelDatas[i];
            return defaultData;
        }
    }

    public FallingModelData this[string label]
    {
        get
        {
            return modelDatas.Find((fmd) => fmd.label.Equals(label)) ?? defaultData;
        }
    }

    public FallingModelData GetRandom()
    {
        return this[Random.Range(0, modelDatas.Count)];
    }

#if UNITY_EDITOR
    [ContextMenu("Auto populate")]
    public void AutoPopulate()
    {
        modelDatas.RemoveAll((o) => o == null);
        if (defaultData != null)
        {
            string defaultPath = AssetDatabase.GetAssetPath(defaultData);
            defaultPath = defaultPath.Substring(0, defaultPath.LastIndexOf('/'));
            string[] guids = AssetDatabase.FindAssets("", new[] { defaultPath });
            foreach (string guid in guids)
            {
                Object o0 = AssetDatabase.LoadAssetAtPath<Object>(AssetDatabase.GUIDToAssetPath(guid));
                if (o0 is FallingModelData && !modelDatas.Contains(o0 as FallingModelData))
                {
                    modelDatas.Add(o0 as FallingModelData);
                }
            }
        }
    }
#endif
}
