﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(fileName = "Falling model data", menuName = "Game/Falling model data")]
public class FallingModelData : ScriptableObject
{
    public string label = "falling-object-label";
    public Mesh model;
    public Material material;
    public Vector3 scale = Vector3.one;
    public Vector3 shadowScale = Vector3.one;
    public float yRotation;
    public Vector2 xySpins;
    public float yPierceEnd;
    public Vector3 colliderCenter;
    public Vector3 colliderSize = Vector3.one;
    public Vector3 trailPosition = new Vector3(0, 1, 0);
    public float trailYRot;
    public float trailSize = 0.5f;
    public Color trailColor = new Color(1, 1, 1, 0.5f);
}

#if UNITY_EDITOR
[CustomEditor(typeof(FallingModelData))]
public class FallingModelDataEditor : Editor
{
    PreviewRenderUtility pru;
    GameObject previewObject;
    GameObject previewShadow;
    GameObject previewTrail;
    GameObject previewCollider;
    Vector3 pivot = new Vector3(0, 2, 0);
    float pitch;
    float yaw;
    float dist = 5;
    bool drawCollider;
    bool init = false;

    public void Init()
    {
        init = true;

        pru = new PreviewRenderUtility();

        pru.camera.nearClipPlane = 0.1f;
        pru.camera.farClipPlane = 20;
        pru.camera.fieldOfView = 70;
        pru.camera.clearFlags = CameraClearFlags.Color;
        Vector3 cpos = Quaternion.Euler(pitch, yaw, 0) * new Vector3(0, 0, -dist) + pivot;
        pru.camera.transform.position = cpos;
        pru.camera.transform.LookAt(pivot);
        pru.lights[0].transform.rotation = pru.camera.transform.rotation;

        previewObject = new GameObject("Preview falling object", typeof(MeshFilter), typeof(MeshRenderer));
        previewTrail = new GameObject("Preview trail", typeof(SpriteRenderer));
        previewTrail.GetComponent<SpriteRenderer>().sprite = AssetDatabase.LoadAssetAtPath<Sprite>("Assets/Textures/pixel.png");
        previewTrail.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 0.25f);
        previewTrail.transform.SetParent(previewObject.transform);
        pru.AddSingleGO(previewObject);

        previewShadow = new GameObject("Preview shadow", typeof(SpriteRenderer));
        previewShadow.GetComponent<SpriteRenderer>().sprite = AssetDatabase.LoadAssetAtPath<Sprite>("Assets/Textures/Game/shadow-flat.png");
        previewShadow.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 0.5f);
        pru.AddSingleGO(previewShadow);

        previewCollider = GameObject.CreatePrimitive(PrimitiveType.Cube);
        pru.AddSingleGO(previewCollider);
        previewCollider.transform.SetParent(previewObject.transform);
    }

    public override void OnInspectorGUI()
    {
        if (!init) Init();

        GUI.enabled = false;
        EditorGUILayout.ObjectField("Script", MonoScript.FromScriptableObject((FallingModelData)target), typeof(FallingModelData), false);
        GUI.enabled = true;

        FallingModelData t = target as FallingModelData;

        EditorGUI.BeginChangeCheck();

        t.label = EditorGUILayout.TextField("Label", t.label);
        EditorGUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        if (GUILayout.Button("Auto acquire", EditorStyles.miniButton, GUILayout.ExpandWidth(false)))
        {
            t.model = AssetDatabase.LoadAssetAtPath<Mesh>("Assets/Models/Falling objects/fo-" + t.label + ".fbx");
            t.material = AssetDatabase.LoadAssetAtPath<Material>("Assets/Materials/Falling objects/fo-" + t.label + ".mat");
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Space();
        t.model = (Mesh)EditorGUILayout.ObjectField("Model", t.model, typeof(Mesh), false);
        t.material = (Material)EditorGUILayout.ObjectField("Material", t.material, typeof(Material), false);

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Pos & rot & scale", EditorStyles.boldLabel);
        EditorGUI.indentLevel++;
        {
            t.scale = EditorGUILayout.Vector3Field("Object scale", t.scale);
            t.shadowScale = EditorGUILayout.Vector3Field("Shadow scale", t.shadowScale);
            t.yRotation = EditorGUILayout.Slider("Y rotation", t.yRotation, 0, 360);
            t.yPierceEnd = EditorGUILayout.FloatField("Y pierce end", t.yPierceEnd);
            t.xySpins = EditorGUILayout.Vector2Field("XY spins", t.xySpins);
            t.colliderCenter = EditorGUILayout.Vector3Field("Collider center", t.colliderCenter);
            t.colliderSize = EditorGUILayout.Vector3Field("Collider size", t.colliderSize);
            t.trailPosition = EditorGUILayout.Vector3Field("Trail position", t.trailPosition);
            t.trailYRot = EditorGUILayout.Slider("Trail Y rotation", t.trailYRot, 0, 360);
            t.trailSize = EditorGUILayout.FloatField("Trail size", t.trailSize);
            t.trailColor = EditorGUILayout.ColorField("Trail color", t.trailColor);
        }
        EditorGUI.indentLevel--;

        if (EditorGUI.EndChangeCheck())
        {
            EditorUtility.SetDirty(t);
        }
    }

    public override bool HasPreviewGUI()
    {
        return true;
    }

    public override void OnPreviewSettings()
    {
        base.OnPreviewSettings();
        drawCollider = EditorGUILayout.ToggleLeft("", drawCollider, GUILayout.MaxWidth(30));
        pivot.y = EditorGUILayout.Slider("", pivot.y, 0, 5, GUILayout.MaxWidth(150));
        pru.camera.transform.LookAt(pivot);
    }

    public override void OnInteractivePreviewGUI(Rect r, GUIStyle background)
    {
        if (Application.isPlaying) return;

        FallingModelData t = target as FallingModelData;

        if (t != null && t.material != null && t.model != null && pru != null)
        {
            previewObject.GetComponent<MeshFilter>().mesh = t.model;
            previewObject.GetComponent<MeshRenderer>().material = t.material;

            if (Event.current.type == EventType.MouseDrag)
            {
                Vector2 delta = Event.current.delta;
                if (Event.current.control)
                {
                    dist += delta.y * 0.025f;
                    dist = Mathf.Clamp(dist, pru.camera.nearClipPlane + 0.5f, pru.camera.farClipPlane);
                }
                else
                {
                    pitch += delta.y;
                    yaw += delta.x;
                    pitch = Mathf.Clamp(pitch, -90, 90);
                }
                Vector3 cpos = Quaternion.Euler(pitch, yaw, 0) * new Vector3(0, 0, -dist) + pivot;

                pru.camera.transform.position = cpos;
                pru.camera.transform.LookAt(pivot);
                pru.lights[0].transform.rotation = pru.camera.transform.rotation;

                Event.current.Use();
            }

            pru.BeginPreview(r, background);

            Vector3 pos = -t.model.bounds.center;
            pos.y -= t.yPierceEnd * t.scale.y;
            previewObject.transform.position = pos;
            previewObject.transform.localScale = t.scale;
            previewObject.transform.rotation = Quaternion.Euler(0, t.yRotation, 0);

            previewShadow.transform.localScale = t.shadowScale;
            previewShadow.transform.rotation = Quaternion.Euler(90, t.yRotation, 0);

            previewTrail.transform.localPosition = t.trailPosition + new Vector3(0, 2, 0);
            previewTrail.transform.localScale = new Vector3(t.trailSize / t.scale.z, 4, 1);
            previewTrail.transform.localEulerAngles = new Vector3(0, t.trailYRot, 0);
            previewTrail.GetComponent<SpriteRenderer>().color = t.trailColor;

            previewCollider.transform.localPosition = t.colliderCenter;
            previewCollider.transform.localScale = t.colliderSize;
            previewCollider.SetActive(drawCollider);

            pru.camera.Render();
            pru.EndAndDrawPreview(r);
        }
    }

    private void OnDestroy()
    {
        if (pru != null) pru.Cleanup();
    }
}

#endif