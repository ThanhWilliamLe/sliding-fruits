﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Juice market", menuName = "Game/Juice market", order = 66)]
class JuiceMarket : ScriptableObject
{
    private JuiceMarket() { }
    private static JuiceMarket instance;
    public static JuiceMarket Instance
    {
        get
        {
            if (instance == null) instance = Resources.Load<JuiceMarket>("Juice market");
            return instance;
        }
    }

    public List<JuiceMarketEntry> entries = new List<JuiceMarketEntry>();
    public List<JuiceTypeSprite> sprites = new List<JuiceTypeSprite>();

    private void OnEnable()
    {
        SortEntries();
    }

    [ContextMenu("Sort")]
    public void SortEntries()
    {
        entries.Sort((j0, j1) =>
        {
            return j0.type.CompareTo(j1.type) * 1000 +
                    j0.videoType.CompareTo(j1.videoType) * 100 +
                    j0.cost.CompareTo(j1.cost) * 10;
        });
        ObjectUtils.MarkDirty(this);
    }

    public Sprite FindSpriteForEntry(JuiceMarketEntry entry)
    {
        JuiceTypeSprite jts = sprites.Find((o) => o.type.Equals(entry.type));
        if (jts != null) return jts.sprite;
        return null;
    }
}

[System.Serializable]
public class JuiceMarketEntry
{
    public string label;
    public string desc;
    public VideoType videoType = VideoType.None;
    public int cost;
    public float amount;
    public Type type;

    public enum Type
    {
        BurstJuice, RecoverJuice, InfiniteJuice, BuyJuice
    }

    public enum VideoType
    {
        Short, Long, None
    }
}

[System.Serializable]
public class JuiceTypeSprite
{
    public JuiceMarketEntry.Type type;
    public Sprite sprite;
}