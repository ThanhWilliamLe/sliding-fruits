﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(fileName = "Fruit model resources", menuName = "Game/Fruit model resources", order = 50)]
public class FruitModelResources : ScriptableObject
{
    private static FruitModelResources instance;
    public static FruitModelResources Instance
    {
        get
        {
            if (instance == null) instance = Resources.Load<FruitModelResources>("Fruit model resources");
            return instance;
        }
    }

    public FruitModelData defaultData;
    public List<FruitModelData> modelDatas = new List<FruitModelData>();

    public FruitModelData this[int i]
    {
        get
        {
            if (i >= 0 && i < modelDatas.Count) return modelDatas[i];
            return defaultData;
        }
    }

    public FruitModelData this[string label]
    {
        get
        {
            return modelDatas.Find((fmd) => fmd.label.Equals(label)) ?? defaultData;
        }
    }

    public FruitModelData GetRandom()
    {
        return this[Random.Range(0, modelDatas.Count)];
    }

#if UNITY_EDITOR
    [ContextMenu("Auto populate")]
    public void AutoPopulate()
    {
        modelDatas.RemoveAll((o) => o == null);
        if (defaultData != null)
        {
            string defaultPath = AssetDatabase.GetAssetPath(defaultData);
            defaultPath = defaultPath.Substring(0, defaultPath.LastIndexOf('/'));
            string[] guids = AssetDatabase.FindAssets("", new[] { defaultPath });
            foreach (string guid in guids)
            {
                Object o0 = AssetDatabase.LoadAssetAtPath<Object>(AssetDatabase.GUIDToAssetPath(guid));
                if (o0 is FruitModelData && !modelDatas.Contains(o0 as FruitModelData))
                {
                    modelDatas.Add(o0 as FruitModelData);
                }
            }
        }
    }
#endif
}
