﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(fileName = "Spawn entries", menuName = "Game/Spawn entries", order = 60)]
public class SpawnEntries : ScriptableObject
{
    private static SpawnEntries instance;
    public static SpawnEntries Instance
    {
        get
        {
            if (instance == null) instance = Resources.Load<SpawnEntries>("Spawn entries");
            return instance;
        }
    }

    public List<SpawnEntry> fallingEntries = new List<SpawnEntry>();

    public SpawnEntry GetRandomFallingAt(float z)
    {
        List<SpawnEntry> l = GetFallingEntriesAt(z);
        return l[Random.Range(0, l.Count)];
    }

    public List<SpawnEntry> GetFallingEntriesAt(float z)
    {
        List<SpawnEntry> l = new List<SpawnEntry>();
        foreach (SpawnEntry se in fallingEntries)
        {
            if (z >= se.spawnFrom) l.Add(se);
        }
        return l;
    }

}

[System.Serializable]
public class SpawnEntry
{
    public FallingModelData fmd;
    public float spawnFrom;
}
