﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(fileName = "Fruit model data", menuName = "Game/Fruit model data", order = 51)]
public class FruitModelData : ScriptableObject
{
    public string label = "fruit-object-label";
    public Mesh model;
    public Material material;
    public Color particleColor;
    public Color trailColor;
    public Color uiColor;
    public Color floorColor;
    public float trailWidth = 1;
    public Vector3 scale = Vector3.one;
    public Vector3 shadowScale = Vector3.one;
    public float yRotation;
    public Vector3 colliderCenter;
    public Vector3 colliderSize = Vector3.one;
    public JuiceContent juiceContent;
}

[System.Serializable]
public class JuiceContent
{
    public float startJuice = 20;
    public float maxJuice = 30;
    public float juiceBurst = 20;
    public float juiceRecover = 1;
    public float juiceLowWarning = 8;
}

#if UNITY_EDITOR
[CustomEditor(typeof(FruitModelData))]
public class FruitModelDataEditor : Editor
{
    PreviewRenderUtility pru;
    GameObject previewObject;
    GameObject previewShadow;
    SpriteRenderer previewParticle;
    SpriteRenderer previewTrail;
    GameObject previewCollider;
    Vector3 pivot = new Vector3(0, 0, 0);
    float pitch;
    float yaw;
    float dist = 3;
    bool drawCollider;
    bool init = false;

    public void Init()
    {
        init = true;

        pitch = Camera.main.transform.rotation.eulerAngles.x;

        pru = new PreviewRenderUtility();
        pru.camera.nearClipPlane = 0.1f;
        pru.camera.farClipPlane = 20;
        pru.camera.fieldOfView = 70;
        pru.camera.clearFlags = CameraClearFlags.Color;
        Vector3 cpos = Quaternion.Euler(pitch, yaw, 0) * new Vector3(0, 0, -dist) + pivot;
        pru.camera.transform.position = cpos;
        pru.camera.transform.LookAt(pivot);
        pru.lights[0].transform.rotation = pru.camera.transform.rotation;

        previewObject = new GameObject("Preview fruit", typeof(MeshFilter), typeof(MeshRenderer));
        pru.AddSingleGO(previewObject);

        previewShadow = new GameObject("Preview shadow", typeof(SpriteRenderer));
        previewShadow.transform.position = new Vector3(0, 0.005f, 0);
        previewShadow.GetComponent<SpriteRenderer>().sprite = AssetDatabase.LoadAssetAtPath<Sprite>("Assets/Textures/Game/shadow-flat.png");
        previewShadow.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 0.5f);
        pru.AddSingleGO(previewShadow);

        previewParticle = new GameObject("Preview particle").AddComponent<SpriteRenderer>();
        previewParticle.transform.rotation = Quaternion.Euler(90, 0, 0);
        previewParticle.transform.position = new Vector3(1, 0.004f, -0.25f);
        previewParticle.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        previewParticle.sprite = AssetDatabase.LoadAssetAtPath<Sprite>("Assets/Textures/Game/shadow-flat.png");
        pru.AddSingleGO(previewParticle.gameObject);

        previewTrail = new GameObject("Preview trail").AddComponent<SpriteRenderer>();
        previewTrail.transform.rotation = Quaternion.Euler(90, 0, 0);
        previewTrail.transform.position = new Vector3(1, 0, 0);
        previewTrail.transform.localScale = new Vector3(2, 1, 1);
        previewTrail.sprite = AssetDatabase.LoadAssetAtPath<Sprite>("Assets/Textures/pixel.png");
        pru.AddSingleGO(previewTrail.gameObject);

        previewCollider = GameObject.CreatePrimitive(PrimitiveType.Cube);
        pru.AddSingleGO(previewCollider);
        previewCollider.transform.SetParent(previewObject.transform);
    }

    public override void OnInspectorGUI()
    {
        if (!init) Init();

        GUI.enabled = false;
        EditorGUILayout.ObjectField("Script", MonoScript.FromScriptableObject((FruitModelData)target), typeof(FruitModelData), false);
        GUI.enabled = true;

        FruitModelData t = target as FruitModelData;

        serializedObject.UpdateIfRequiredOrScript();
        EditorGUI.BeginChangeCheck();

        t.label = EditorGUILayout.TextField("Label", t.label);
        EditorGUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        if (GUILayout.Button("Auto acquire", EditorStyles.miniButton, GUILayout.ExpandWidth(false)))
        {
            t.model = AssetDatabase.LoadAssetAtPath<Mesh>("Assets/Models/Fruits/fr-" + t.label + ".fbx");
            t.material = AssetDatabase.LoadAssetAtPath<Material>("Assets/Materials/Fruits/fr-" + t.label + ".mat");
            if (t.material != null)
            {
                Texture tex = t.material.GetTexture("_MainTex");
                if (tex != null)
                {
                    float s = 0.05f;
                    Color shade = new Color(s, s, s, 0);
                    Color col = GetTextureMainColor(tex);
                    t.particleColor = col - shade;
                    t.trailColor = col + shade;
                    t.uiColor = col + shade * 5;
                    t.floorColor = InvertHue(col);
                }
            }
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Space();
        t.model = (Mesh)EditorGUILayout.ObjectField("Model", t.model, typeof(Mesh), false);
        t.material = (Material)EditorGUILayout.ObjectField("Material", t.material, typeof(Material), false);
        t.particleColor = EditorGUILayout.ColorField("Particle color", t.particleColor);
        t.trailColor = EditorGUILayout.ColorField("Trail color", t.trailColor);
        t.trailWidth = EditorGUILayout.FloatField("Trail width", t.trailWidth);
        t.uiColor = EditorGUILayout.ColorField("UI color", t.uiColor);
        t.floorColor = EditorGUILayout.ColorField("Floor color", t.floorColor);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("juiceContent"), true);

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Pos & rot & scale", EditorStyles.boldLabel);
        EditorGUI.indentLevel++;
        {
            t.scale = EditorGUILayout.Vector3Field("Object scale", t.scale);
            t.shadowScale = EditorGUILayout.Vector3Field("Shadow scale", t.shadowScale);
            t.yRotation = EditorGUILayout.Slider("Y rotation", t.yRotation, 0, 360);
            t.colliderCenter = EditorGUILayout.Vector3Field("Collider center", t.colliderCenter);
            t.colliderSize = EditorGUILayout.Vector3Field("Collider size", t.colliderSize);
            if (GUILayout.Button("Collider to model") && t.model != null)
            {
                t.colliderCenter = t.model.bounds.center;
                t.colliderSize = t.model.bounds.size;
            }
        }
        EditorGUI.indentLevel--;

        if (EditorGUI.EndChangeCheck())
        {
            EditorUtility.SetDirty(t);
        }

        serializedObject.ApplyModifiedProperties();
    }

    Color GetTextureMainColor(Texture t)
    {
        Texture2D tex = t as Texture2D;
        if (tex != null)
        {
            Dictionary<Color, int> colorCounts = new Dictionary<Color, int>();
            Color[] colors = tex.GetPixels();
            foreach (Color c in colors)
            {
                if (c.a == 0) continue;
                if (!colorCounts.ContainsKey(c)) colorCounts.Add(c, 0);
                colorCounts[c]++;
            }
            Color ret = Color.white;
            int count = 0;
            foreach (KeyValuePair<Color, int> kvp in colorCounts)
            {
                if (kvp.Value > count)
                {
                    ret = kvp.Key;
                    count = kvp.Value;
                }
            }
            return ret;
        }
        return Color.white;
    }

    Color InvertHue(Color c)
    {
        float h = 0;
        float s = 0;
        float v = 0;
        Color.RGBToHSV(c, out h, out s, out v);
        return Color.HSVToRGB((h + 0.5f) % 1f, s, v);
    }

    public override bool HasPreviewGUI()
    {
        return true;
    }

    public override void OnPreviewSettings()
    {
        base.OnPreviewSettings();
        drawCollider = EditorGUILayout.ToggleLeft("", drawCollider, GUILayout.MaxWidth(30));
        pivot.y = EditorGUILayout.Slider("", pivot.y, 0, 5, GUILayout.MaxWidth(150));
        pru.camera.transform.LookAt(pivot);
    }

    public override void OnInteractivePreviewGUI(Rect r, GUIStyle background)
    {
        FruitModelData t = target as FruitModelData;

        if (t != null && t.material != null && t.model != null && pru != null)
        {
            previewObject.GetComponent<MeshFilter>().mesh = t.model;
            previewObject.GetComponent<MeshRenderer>().material = t.material;

            if (Event.current.type == EventType.MouseDrag)
            {
                Vector2 delta = Event.current.delta;
                if (Event.current.control)
                {
                    dist += delta.y * 0.025f;
                    dist = Mathf.Clamp(dist, pru.camera.nearClipPlane + 0.5f, pru.camera.farClipPlane);
                }
                else
                {
                    pitch += delta.y;
                    yaw += delta.x;
                    pitch = Mathf.Clamp(pitch, -90, 90);
                }
                Vector3 cpos = Quaternion.Euler(pitch, yaw, 0) * new Vector3(0, 0, -dist) + pivot;

                pru.camera.transform.position = cpos;
                pru.camera.transform.LookAt(pivot);
                pru.lights[0].transform.rotation = pru.camera.transform.rotation;

                Event.current.Use();
            }

            pru.BeginPreview(r, background);

            Vector3 pos = Vector3.zero;
            pos.y += t.model.bounds.extents.y;
            previewObject.transform.position = pos;
            previewObject.transform.localScale = t.scale;
            previewObject.transform.rotation = Quaternion.Euler(0, t.yRotation, 0);

            previewShadow.transform.localScale = t.shadowScale;
            previewShadow.transform.rotation = Quaternion.Euler(90, t.yRotation, 0);

            previewCollider.transform.localPosition = t.colliderCenter;
            previewCollider.transform.localScale = t.colliderSize;
            previewCollider.SetActive(drawCollider);

            previewParticle.color = t.particleColor;

            previewTrail.color = t.trailColor;
            Vector3 pts = previewTrail.transform.localScale;
            pts.y = t.trailWidth * t.scale.z;
            previewTrail.transform.localScale = pts;

            pru.camera.Render();
            pru.EndAndDrawPreview(r);
        }
    }

    private void OnDestroy()
    {
        if (pru != null) pru.Cleanup();
    }
}

#endif