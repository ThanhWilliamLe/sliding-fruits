﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager
{
    private GameManager()
    {
        Globals gb = Globals.Instance;
    }

    #region Static
    private static GameManager instance;
    public static GameManager Instance
    {
        get
        {
            if (instance == null) instance = new GameManager();
            return instance;
        }
    }

    public static GameController CurrentGame
    {
        get { return Instance.currentGame ?? Object.FindObjectOfType<GameController>(); }
        set { Instance.currentGame = value; }
    }

    public static Player CurrentPlayer
    {
        get { return CurrentGame != null ? CurrentGame.player : Object.FindObjectOfType<Player>(); }
    }

    public static bool IsInGame()
    {
        return CurrentGame != null;
    }

    public static bool IsPlaying()
    {
        return CurrentGame != null && CurrentGame.IsPlaying();
    }
    #endregion

    private GameController currentGame;

    public int Coin
    {
        get { return PlayerPrefs.GetInt("twle.slidingfruits.coin"); }
        set { PlayerPrefs.SetInt("twle.slidingfruits.coin", value); }
    }

    public void StartGame()
    {
        SceneManager.LoadScene("GameScene");
    }

    public void GoHome()
    {
        SceneManager.LoadScene("HomeScene");
    }

}
