﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class PlayerTrailSpawner : MonoBehaviour
{
    public Player player;
    public Transform trailGroup;
    public GameObject trailPrefab;

    PlayerTrail currentTrail;
    Queue<PlayerTrail> trailPool = new Queue<PlayerTrail>();

    private void Reset()
    {
        player = GetComponent<Player>();

        GameObject go = GameObject.Find("Player trails");
        if (go != null) trailGroup = go.transform;

#if UNITY_EDITOR
        trailPrefab = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Prefabs/Player trail.prefab");
#endif
    }

    public void EndTrail()
    {
        if (currentTrail != null)
        {
            currentTrail.SetEndPos(GetTrailPos());
            currentTrail.poolable = true;
        }
        currentTrail = null;
    }

    public void NewTrail(bool first = false)
    {
        if (currentTrail != null) currentTrail.SetEndPos(GetTrailPos());
        currentTrail = UnpoolTrail();
        currentTrail.poolable = false;
        currentTrail.isFirstTrail = first;
        currentTrail.color = player.fmd != null ? player.fmd.trailColor : default(Color);
        currentTrail.SetWidth(player.fmd != null ? player.fmd.trailWidth : 1);
        currentTrail.SetStartPos(GetTrailPos());
        currentTrail.SetEndPos(GetTrailPos());
        currentTrail.InitMesh();
    }

    PlayerTrail UnpoolTrail()
    {
        if (trailPool.Count == 0)
        {
            GameObject go = Instantiate(trailPrefab, trailGroup);
            PlayerTrail trail = go.GetComponent<PlayerTrail>();
            trail.spawner = this;
            trailPool.Enqueue(trail);
        }
        return trailPool.Dequeue();
    }

    public void PoolTrail(PlayerTrail trail)
    {
        trailPool.Enqueue(trail);
    }

    Vector3 GetTrailPos()
    {
        Vector3 v = player.transform.position;
        v.y = 0.002f;
        return v;
    }

    private void Update()
    {
        if (currentTrail != null) currentTrail.SetEndPos(GetTrailPos());
    }
}
