﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer), typeof(MeshFilter), typeof(BoxCollider))]
[RequireComponent(typeof(ParticleSystem), typeof(PlayerTrailSpawner))]
public class Player : MonoBehaviour
{
    [Header("Components")]
    public BoxCollider col;
    public MeshFilter mf;
    public MeshRenderer mr;
    public ParticleSystem ps;
    public PlayerTrailSpawner pts;
    public FruitModelData fmd;
    public PlayerShadow sh;

    [Header("Movements")]
    public float startSpeed = 5;
    public float maxSpeed = 20;
    public float speed = 5;
    public bool dieable = true;

    [Header("Particles")]
    public Vector2 drippingRange = new Vector2(.75f, 1.5f);
    public Vector2Int drippingCountRange = new Vector2Int(1, 3);
    public int switchDrippingCount = 20;
    public int jumpDrippingCount = 50;
    public int deathDrippingCount = 50;

    Vector3 velocity;
    float movedSqrSinceLastDripping;
    bool moving;
    bool moveLeft;
    bool dead;
    float roadWidth = -1;

    float RoadWidth
    {
        get
        {
            if (roadWidth == -1)
            {
                if (GameManager.IsInGame()) roadWidth = GameManager.CurrentGame.roadWidth;
                else
                {
                    HomeController hc = FindObjectOfType<HomeController>();
                    if (hc != null) roadWidth = hc.roadWidth;
                }
            }
            return roadWidth;
        }
    }

    public bool CanBehave
    {
        get { return !dead && GameManager.CurrentGame.IsPlaying(); }
    }

    private void Reset()
    {
        col = GetComponent<BoxCollider>();
        mf = GetComponent<MeshFilter>();
        mr = GetComponent<MeshRenderer>();
        ps = GetComponent<ParticleSystem>();
        pts = GetComponent<PlayerTrailSpawner>();
        sh = FindObjectOfType<PlayerShadow>();
        SetModelData(FruitModelResources.Instance.defaultData, true);
    }

    private void Awake()
    {
        ParticleSystem.CollisionModule colm = ps.collision;
        colm.SetPlane(0, GameObject.Find("Floor").transform);
        speed = startSpeed;
    }

    public void SetModelData(FruitModelData f, bool force)
    {
        if (fmd != f || force)
        {
            fmd = f;
            if (fmd != null)
            {
                ApplyModelData();
            }
        }
    }

    [ContextMenu("Apply current model data")]
    public void ApplyModelData()
    {
        if (fmd.model != null)
        {
            Vector3 v = transform.position;
            v.y = fmd.model.bounds.extents.y;
            transform.position = v;
        }
        if (col != null)
        {
            col.center = fmd.colliderCenter;
            col.size = fmd.colliderSize;
        }
        if (mf != null)
        {
            mf.mesh = fmd.model;
        }
        if (mr != null)
        {
            mr.material = fmd.material;
        }
        if (ps != null)
        {
            ParticleSystem.MainModule main = ps.main;

            float s = 0.1f;
            Color shade = new Color(s, s, s, 0);
            ParticleSystem.MinMaxGradient mmg = main.startColor;
            mmg.colorMin = fmd.particleColor - shade;
            mmg.colorMax = fmd.particleColor + shade;
            main.startColor = mmg;
        }
        if (sh != null)
        {
            Vector3 v = sh.transform.position;
            v.y = 0.005f;
            sh.transform.position = v;
            sh.transform.localScale = fmd.shadowScale;
        }
    }

    public void StartMoving()
    {
        moving = true;
        pts.NewTrail(true);
    }

    private void FixedUpdate()
    {
        if (!dead)
        {
            if (dieable && CheckKnifeCollision(Time.fixedDeltaTime))
            {
                SpawnDeathParticles();
                dead = true;
                GameManager.CurrentGame.OnPlayerDied();
            }
            else if (moving)
            {
                MoveUpdate(Time.fixedDeltaTime);
                if (movedSqrSinceLastDripping >= Random.Range(drippingRange.x, drippingRange.y))
                {
                    SpawnParticles(Random.Range(drippingCountRange.x, drippingCountRange.y));
                    movedSqrSinceLastDripping = 0;
                }

                FruitModelData fmd = CheckRoadFruitCollision(Time.fixedDeltaTime);
                if (fmd != null)
                {
                    SetModelData(fmd, true);
                    GameManager.CurrentGame.PlayerChangedFruit();
                }
            }
        }
    }

    void MoveUpdate(float timePassed)
    {
        if (transform.position.x < -RoadWidth) SwitchDirection(1);
        else if (transform.position.x > RoadWidth) SwitchDirection(-1);

        velocity = new Vector3(moveLeft ? -speed : speed, 0, speed);

        Vector3 move = velocity * timePassed;
        Vector3 pos = transform.position;
        pos += move;
        transform.position = pos;
        movedSqrSinceLastDripping += move.sqrMagnitude;

        float rot = move.x * 45;
        transform.Rotate(0, rot, 0);
    }

    public void SwitchDirection(int force = 0)
    {
        if (dead || !moving) return;

        if (force != 0) moveLeft = force < 0;
        else moveLeft = !moveLeft;
        SpawnParticles(switchDrippingCount);
        pts.NewTrail();
    }

    void SpawnParticles(int i)
    {
        ParticleSystem.ShapeModule shape = ps.shape;
        Vector3 r = shape.rotation;
        r.y = (moveLeft ? 135 : 225) - transform.eulerAngles.y;
        shape.rotation = r;
        ps.Emit(i);
    }

    void SpawnDeathParticles()
    {
        ParticleSystem.ShapeModule shape = ps.shape;
        shape.rotation = new Vector3(-90, -transform.eulerAngles.y, 0);
        ps.Emit(deathDrippingCount);
    }

    bool CheckKnifeCollision(float timePassed)
    {
        Vector3 colCenter = transform.TransformPoint(col.center);
        Vector3 colExtends = col.size / 2f;
        colExtends.Scale(transform.lossyScale);

        if (Physics.CheckBox(colCenter, colExtends / 2f, transform.rotation, LayerMask.GetMask("Fallings")))
            return true;

        return false;
    }

    FruitModelData CheckRoadFruitCollision(float timePassed, bool andDestroy = true)
    {
        Vector3 colCenter = transform.TransformPoint(col.center);
        Vector3 colExtends = col.size / 2f;
        colExtends.Scale(transform.lossyScale);

        Collider[] cols = Physics.OverlapBox(colCenter, colExtends / 2f, transform.rotation, LayerMask.GetMask("RoadFruits"));
        if (cols.Length > 0)
        {
            foreach (Collider c0 in cols)
            {
                RoadFruit rf = c0.gameObject.GetComponent<RoadFruit>();
                if (rf != null && rf.fmd != null)
                {
                    if (andDestroy) Destroy(c0.gameObject);
                    return rf.fmd;
                }
            }
        }

        return null;
    }

    public void Revive()
    {
        dead = false;
        moving = true;
        speed = startSpeed;
    }
}
