﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraObject : MonoBehaviour
{
    public Camera cam;
    public float maxSpeed = 5;
    public bool tracking;

    float zDiff;

    public Player Player
    {
        get { return GameManager.CurrentPlayer; }
    }

    public void Reset()
    {
        cam = GetComponent<Camera>(); ;
    }

    [ContextMenu("Find camera FOV")]
    public void FindCameraFOV()
    {
        float w = 0;
        if (GameManager.IsInGame()) w = GameManager.CurrentGame.roadOuterWidth;
        else
        {
            HomeController hc = FindObjectOfType<HomeController>();
            if (hc != null) w = hc.roadOuterWidth;
        }

        Vector3 pp = Player.transform.position;
        float x0 = pp.x - w;
        float x1 = pp.x + w;

        Vector2 sp0 = cam.WorldToViewportPoint(new Vector3(x0, pp.y, pp.z));
        Vector2 sp1 = cam.WorldToViewportPoint(new Vector3(x1, pp.y, pp.z));

        float spSize = Mathf.Abs(sp1.x - sp0.x);
        cam.fieldOfView *= spSize;
    }

    public void ApplyFMD(FruitModelData fmd)
    {
        if (fmd != null) cam.backgroundColor = fmd.floorColor;
    }

    private void Start()
    {
        StartTracking();
    }

    public void StartTracking()
    {
        zDiff = transform.position.z - Player.transform.position.z;
        tracking = true;
    }

    private void Update()
    {
        if (tracking)
        {
            Vector3 camPos = transform.position;

            float ztarget = Player.transform.position.z + zDiff;
            float zdiff = ztarget - camPos.z;
            //float zmove = Mathf.Clamp(zdiff, 0, maxSpeed);

            camPos.z = camPos.z + zdiff;
            transform.position = camPos;
        }
    }
}
