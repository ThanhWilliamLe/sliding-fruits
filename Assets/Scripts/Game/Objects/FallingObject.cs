﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer), typeof(MeshFilter), typeof(BoxCollider))]
public class FallingObject : MonoBehaviour
{
    public MeshFilter mf;
    public MeshRenderer mr;
    public BoxCollider bc;
    public TrailRenderer tr;
    public FallingModelData fmd;
    public FallingShadow fs;
    public bool behaving;

    Vector3 velocity;
    bool stuckToGround;
    bool dieable;
    float timeTillGround;
    float spinRotX;
    float spinRotY;

    public bool CanBehave
    {
        get { return behaving; }
    }

    private void Awake()
    {
        fmd = null;
    }

    private void Reset()
    {
        mf = GetComponent<MeshFilter>();
        mr = GetComponent<MeshRenderer>();
        bc = GetComponent<BoxCollider>();
        tr = GetComponentInChildren<TrailRenderer>();
        SetFMD(FallingModelResources.Instance.defaultData);
    }

    public void OnSpawn(FallingModelData f, float t)
    {
        behaving = true;
        dieable = false;
        stuckToGround = false;
        velocity = Vector3.zero;
        timeTillGround = t;
        SetFMD(f);
        ResetSpinRotation();
        tr.enabled = true;
    }

    public void SetFMD(FallingModelData f)
    {
        if (fmd != f)
        {
            fmd = f;
            ApplyFMD();
        }
    }

    [ContextMenu("Apply current fmd")]
    public void ApplyFMD()
    {
        if (fmd != null)
        {
            mf.mesh = fmd.model;
            mr.material = fmd.material;
            transform.localScale = fmd.scale;
            transform.localEulerAngles = new Vector3(0, fmd.yRotation, 0);
            bc.center = fmd.colliderCenter;
            bc.size = fmd.colliderSize;
            tr.transform.localEulerAngles = new Vector3(0, fmd.trailYRot, 0);
            tr.transform.localPosition = fmd.trailPosition;
            tr.widthMultiplier = fmd.trailSize / fmd.scale.z;
            tr.startColor = tr.endColor = fmd.trailColor;
        }
    }

    public void ResetSpinRotation()
    {
        if (fmd != null)
        {
            spinRotX = fmd.xySpins.x * timeTillGround;
            spinRotY = fmd.xySpins.y * timeTillGround;
        }
    }

    void UpdateRotFromSpin()
    {
        transform.localRotation = Quaternion.Euler(spinRotX, fmd.yRotation + spinRotY, 0);
    }

    private void FixedUpdate()
    {
        if (!CanBehave) return;

        if (dieable && !mr.isVisible)
        {
            Pool();
            return;
        }

        if (!stuckToGround)
        {
            float deltaTime = Time.fixedDeltaTime;

            velocity.y += GameManager.CurrentGame.gravity * deltaTime;

            Vector3 pos = transform.position;
            pos.y += velocity.y * deltaTime;
            transform.position = pos;

            spinRotX = Mathf.Max(0, spinRotX - fmd.xySpins.x * deltaTime);
            spinRotY = Mathf.Max(0, spinRotY - fmd.xySpins.y * deltaTime);
            UpdateRotFromSpin();

            if (GetPierceEndPosition().y <= 0)
            {
                pos.y = -GetPierceEndDiffFromCenter();
                transform.position = pos;
                velocity = Vector3.zero;
                stuckToGround = true;
                dieable = true;
                GameManager.CurrentGame.fo.KnifePierceAt(transform.position);
            }
        }
        else if (spinRotX > 0)
        {
            spinRotX = 0;
            UpdateRotFromSpin();
        }
    }

    public void Pool(Vector3? poolPos = null)
    {
        behaving = false;
        tr.enabled = false;
        GameManager.CurrentGame.sc.PoolFalling(gameObject);
        if (poolPos.HasValue) transform.position = poolPos.Value;
    }

    public Vector3 GetPierceEndPosition()
    {
        return transform.position + new Vector3(0, GetPierceEndDiffFromCenter(), 0);
    }

    public float GetPierceEndDiffFromCenter()
    {
        return fmd.yPierceEnd * transform.localScale.y;
    }

}
