﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class FallingShadow : MonoBehaviour
{
    public SpriteRenderer sr;
    public FallingObject fallingObj;
    public Vector2 alphaRange = new Vector2(0, 1);
    public float fallFromHeight;
    public bool behaving;

    private void Reset()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    public void OnSpawn(FallingObject falling)
    {
        falling.fs = this;

        behaving = true;
        fallingObj = falling;
        fallFromHeight = fallingObj.transform.position.y;

        Vector3 pos = fallingObj.transform.position;
        pos.y = 0.004f;
        transform.position = pos;
    }

    private void Update()
    {
        if (!behaving) return;

        if (fallingObj == null || fallingObj.gameObject == null || !fallingObj.behaving)
        {
            Pool();
            return;
        }

        float fellRatio = Mathf.Clamp01(1 - fallingObj.transform.position.y / fallFromHeight);
        if (float.IsNaN(fellRatio)) return;

        float alphaRatio = Mathf.Pow(fellRatio, 0.3f);
        float sizeRatio = Mathf.Pow(fellRatio, 0.3f);
        sizeRatio = 1;
        //alphaRatio = 1;

        Color c = sr.color;
        c.a = Mathf.Lerp(alphaRatio, alphaRange.x, alphaRange.y);
        sr.color = c;
        transform.localScale = fallingObj.fmd.shadowScale * sizeRatio;

        Vector3 pos = fallingObj.transform.position;
        pos.y = transform.position.y;
        transform.position = pos;
        transform.localEulerAngles = new Vector3(90, fallingObj.transform.localEulerAngles.y, 0);
    }

    public void Pool(Vector3? poolPos = null)
    {
        if (fallingObj != null && fallingObj.fs == this) fallingObj.fs = null;
        behaving = false;
        GameManager.CurrentGame.sc.PoolShadow(gameObject);
        if (poolPos.HasValue) transform.position = poolPos.Value;
    }
}
