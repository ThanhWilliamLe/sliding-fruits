﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTrail : MonoBehaviour
{
    public MeshFilter mf;
    public Color color;

    [HideInInspector]
    public PlayerTrailSpawner spawner;
    public bool isFirstTrail;
    public bool poolable;

    float width;
    Vector3 startPos;
    Vector3 endPos;

    bool dirty;

    private void Reset()
    {
        mf = GetComponent<MeshFilter>();
    }

    public void InitMesh()
    {
        Mesh m = mf.mesh ?? new Mesh();
        m.vertices = new[] { Vector3.zero, Vector3.zero, Vector3.zero, Vector3.zero };
        m.normals = new[] { Vector3.up, Vector3.up, Vector3.up, Vector3.up };
        if (isFirstTrail)
        {
            Color c0 = color;
            c0.a = 0;
            m.colors = new[] { c0, c0, color, color };
        }
        else m.colors = new[] { color, color, color, color };
        m.triangles = new[] { 0, 1, 2, 2, 3, 0 };
        mf.mesh = m;
    }

    public void SetStartPos(Vector3 v)
    {
        if (!startPos.Equals(v))
        {
            startPos = v;
            transform.position = startPos;
            dirty = true;
        }
    }

    public void SetEndPos(Vector3 v)
    {
        if (!endPos.Equals(v))
        {
            endPos = v;
            dirty = true;
        }
    }

    public void SetWidth(float f)
    {
        if (width != f)
        {
            width = f;
            dirty = true;
        }
    }

    private void OnBecameInvisible()
    {
        if (poolable) spawner.PoolTrail(this);
    }

    private void Update()
    {
        if (dirty)
        {
            UpdateVerts();
        }
    }

    public void UpdateVerts()
    {
        Vector3[] verts = new[]
        {
            new Vector3(width/2,0,0),
             new Vector3(-width/2,0,0),
            endPos - startPos + new Vector3(-width/2,0,0),
            endPos - startPos + new Vector3(width/2,0,0)
        };
        mf.mesh.vertices = verts;
        mf.mesh.RecalculateBounds();
        dirty = false;
    }
}

