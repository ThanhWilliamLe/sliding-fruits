﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideLineObject : MonoBehaviour
{
    public MeshFilter mf;
    public MeshRenderer mr;
    public bool isLeft;

    float RoadWidth
    {
        get
        {
            if (GameManager.IsInGame()) return GameManager.CurrentGame.roadWidth;
            else
            {
                HomeController hc = FindObjectOfType<HomeController>();
                if (hc != null) return hc.roadWidth;
            }
            return 1;
        }
    }

    private void Reset()
    {
        mf = GetComponent<MeshFilter>();
        mr = GetComponent<MeshRenderer>();
    }

    public void ApplyFMD(FruitModelData fmd)
    {
        if (fmd == null) return;

        Color f = fmd.uiColor;
        //f += new Color(1, 1, 1, 0) * 0.15f;

        Mesh m = null;
        if (Application.isEditor && !Application.isPlaying) m = mf.sharedMesh;
        else m = mf.mesh;
        if (m != null)
        {
            Color[] cls = new Color[m.vertexCount];
            for (int i = 0; i < cls.Length; i++) cls[i] = f;
            m.colors = cls;
        }
    }

    [ContextMenu("Move to place")]
    public void MoveToPlace()
    {
        if (isLeft) MoveToLeft();
        else MoveToRight();
    }

    public void MoveToLeft()
    {
        Vector3 v = transform.position;
        v.x = -RoadWidth;
        transform.position = v;
        //Scale();
    }

    public void MoveToRight()
    {
        Vector3 v = transform.position;
        v.x = RoadWidth;
        transform.position = v;
        //Scale();
    }

    public void Scale()
    {
        SetGlobalScale(Vector3.one);
        transform.localScale = new Vector3(transform.localScale.x, 1, transform.localScale.z);
    }

    public void SetGlobalScale(Vector3 globalScale)
    {
        transform.localScale = Vector3.one;
        transform.localScale = new Vector3(globalScale.x / transform.lossyScale.x, globalScale.y / transform.lossyScale.y, globalScale.z / transform.lossyScale.z);
    }
}
