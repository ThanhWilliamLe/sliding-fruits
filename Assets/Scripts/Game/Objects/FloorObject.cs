﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloorObject : MonoBehaviour
{
    public MeshFilter mf;
    public MeshRenderer mr;
    public ParticleSystem ps;
    public bool tracking;
    float zDiff;

    public Player Player
    {
        get { return GameManager.CurrentPlayer; }
    }

    private void Reset()
    {
        mf = GetComponent<MeshFilter>();
        mr = GetComponent<MeshRenderer>();
        ps = GetComponentInChildren<ParticleSystem>();
    }

    [ContextMenu("Fill camera")]
    public void FillCamera()
    {
        Camera cam = null;
        if (GameManager.IsInGame()) cam = GameManager.CurrentGame.co.cam;
        else cam = Camera.main;

        Ray r00 = cam.ViewportPointToRay(new Vector3(0, 0));
        Ray r10 = cam.ViewportPointToRay(new Vector3(1, 0));
        Ray r01 = cam.ViewportPointToRay(new Vector3(0, 1));
        Ray r11 = cam.ViewportPointToRay(new Vector3(1, 1));

        Vector3 v00 = r00.origin + r00.direction * (-r00.origin.y / r00.direction.y);
        Vector3 v10 = r10.origin + r10.direction * (-r10.origin.y / r10.direction.y);
        Vector3 v01 = r01.origin + r01.direction * (-r01.origin.y / r01.direction.y);
        Vector3 v11 = r11.origin + r11.direction * (-r11.origin.y / r11.direction.y);

        Vector3 center = (v00 + v10 + v01 + v11) / 4f;
        float xSize = Mathf.Max(Mathf.Abs(v00.x - v10.x), Mathf.Abs(v01.x - v11.x));
        float zSize = Mathf.Max(Mathf.Abs(v00.z - v01.z), Mathf.Abs(v10.z - v11.z));
        center.y = transform.position.y;

        transform.position = center;
        transform.localScale = new Vector3(xSize, transform.localScale.y, zSize);
    }

    public void ApplyFMD(FruitModelData fmd)
    {
        if (fmd == null) return;

        Mesh m = null;
        if (Application.isEditor && !Application.isPlaying) m = mf.sharedMesh;
        else m = mf.mesh;
        if (m != null)
        {
            Color[] cls = new Color[m.vertexCount];
            for (int i = 0; i < cls.Length; i++) cls[i] = fmd.floorColor;
            m.colors = cls;
        }

        Color shade = new Color(1, 1, 1, 0) * 0.1f;
        ParticleSystem.MainModule main = ps.main;
        main.startColor = fmd.floorColor - shade;

        //Material mat = null;
        //if (Application.isEditor && !Application.isPlaying) mat = mr.sharedMaterial;
        //else mat = mr.material;
        //if (mat != null)
        //{
        //    mat.SetColor("_Color", fmd.floorColor);
        //}
    }

    public void StartTracking()
    {
        zDiff = transform.position.z - Player.transform.position.z;
        tracking = true;
    }

    private void Update()
    {
        if (tracking)
        {
            Vector3 pos = transform.position;
            pos.z = Player.transform.position.z + zDiff;
            transform.position = pos;
        }
    }

    public void KnifePierceAt(Vector3 pos)
    {
        ps.transform.position = new Vector3(pos.x, ps.transform.position.y, pos.z);
        ps.Emit(10);
    }

    public void BoomKnifeAt(Vector3 pos)
    {
        ps.transform.position = new Vector3(pos.x, pos.y, pos.z);
        ps.Emit(50);
    }
}
