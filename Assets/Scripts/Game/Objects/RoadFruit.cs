﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadFruit : MonoBehaviour
{
    public BoxCollider col;
    public MeshFilter mf;
    public MeshRenderer mr;
    public Transform sh;
    public FruitModelData fmd;
    public float animTime = 1f;
    public float rotationSpeed = 360;
    public float shift = 0.25f;

    private void Reset()
    {
        col = GetComponent<BoxCollider>();
        mf = GetComponent<MeshFilter>();
        mr = GetComponent<MeshRenderer>();
        sh = transform.Find("Shadow");
        SetModelData(FruitModelResources.Instance.defaultData);
    }

    public void SetModelData(FruitModelData f)
    {
        fmd = f;
        if (fmd == null) return;

        if (fmd.model != null)
        {
            Vector3 v = transform.position;
            v.y = fmd.model.bounds.extents.y + shift;
            transform.position = v;
        }
        if (col != null)
        {
            col.center = fmd.colliderCenter;
            col.size = fmd.colliderSize + new Vector3(0, shift * 2, 0);
        }
        if (mf != null)
        {
            mf.mesh = fmd.model;
        }
        if (mr != null)
        {
            mr.material = fmd.material;
        }
        if (sh != null)
        {
            Vector3 v = sh.position;
            v.y = 0.005f;
            sh.position = v;
            sh.localScale = fmd.shadowScale;
        }
    }

    public void Spawn(float delay)
    {
        StartCoroutine(SpawnAnim(delay));
    }

    void Update()
    {
        transform.Rotate(0, rotationSpeed * Time.deltaTime, 0);
        if (!mr.isVisible && transform.position.z < GameManager.CurrentPlayer.transform.position.z)
        {
            Destroy(gameObject);
        }
    }

    IEnumerator SpawnAnim(float delay)
    {
        sh.SetParent(null);

        Vector3 oriScale = transform.localScale;
        Vector3 shOriScale = sh.localScale;

        float time = 0;
        while (time < animTime)
        {
            Vector3 shRot = sh.localEulerAngles;
            shRot.y = transform.localEulerAngles.y;
            sh.localEulerAngles = shRot;

            float ratio = time / animTime;
            transform.localScale = Vector3.Lerp(Vector3.zero, oriScale, ratio);
            sh.localScale = Vector3.Lerp(Vector3.zero, shOriScale, ratio);

            if (time == 0) yield return new WaitForSeconds(delay);
            yield return null;
            time += Mathf.Min(Time.deltaTime, animTime - time);
        }
        sh.SetParent(transform);
    }
}
