﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class UIController : MonoBehaviour
{
    public ScreenFlash screenFlash;
    public GameUI gameUI;
    public JuiceUI juiceUI;
    public PauseUI pauseUI;
    public AskAdsUI askAdsUI;
    public GameOverUI gameOverUI;

    private void Reset()
    {
        gameUI = GetComponentInChildren<GameUI>(true);
        juiceUI = GetComponentInChildren<JuiceUI>(true);
        pauseUI = GetComponentInChildren<PauseUI>(true);
        gameOverUI = GetComponentInChildren<GameOverUI>(true);
        screenFlash = GetComponentInChildren<ScreenFlash>(true);
    }

    public void Flash(Color c, System.Action callback)
    {
        screenFlash.Flash(c, callback);
    }

    public void ApplyFMD(FruitModelData fmd)
    {
        gameUI.ApplyFMD(fmd);
        juiceUI.ApplyFMD(fmd);
        pauseUI.ApplyFMD(fmd);
        gameOverUI.ApplyFMD(fmd);
    }

    public void OnGameOver()
    {
        if (GameManager.CurrentGame.canHaveSecondChance && Advertisement.IsReady("rewardedVideo"))
        {
            askAdsUI.gameObject.SetActive(true);
            askAdsUI.Show(() =>
            {
                askAdsUI.gameObject.SetActive(false);
                ShowGameOver();
            },
            () =>
            {
                askAdsUI.gameObject.SetActive(false);
                OnRewardAdsWatched();
            });
        }
        else ShowGameOver(1f);
    }

    public void OnRewardAdsWatched()
    {
        GameManager.CurrentGame.GiveSecondChance();
    }

    public void ShowGameOver(float time = 0)
    {
        Invoke("ReallyShowGameOver", time);
    }

    public void ReallyShowGameOver()
    {
        gameOverUI.endGameCoin = GameManager.Instance.Coin;
        gameOverUI.endGameScore = GameManager.CurrentGame.Score;
        gameOverUI.gameObject.SetActive(true);
        GameManager.CurrentGame.AddScoreToCoin();
    }

    public void OnPauseGame()
    {
        pauseUI.gameObject.SetActive(true);
    }

    public void OnUnpauseGame()
    {
        pauseUI.gameObject.SetActive(false);
    }

    public void ShowJuiceUI()
    {
        juiceUI.gameObject.SetActive(true);
    }

    public void HideJuiceUI()
    {
        juiceUI.gameObject.SetActive(false);
    }
}
