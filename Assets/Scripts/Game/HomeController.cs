﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HomeController : MonoBehaviour
{
    public FloorObject fo;
    public CameraObject co;
    public Player player;
    public SideLineObject[] slos;
    public float roadWidth = 10;
    public float roadOuterWidth = 12;

    private void Reset()
    {
        fo = FindObjectOfType<FloorObject>();
        co = FindObjectOfType<CameraObject>();
        player = FindObjectOfType<Player>();
        slos = FindObjectsOfType<SideLineObject>();
    }

    private void Start()
    {
        InitGame();
        StartGame();
    }

    [ContextMenu("Init game")]
    public void InitGame()
    {
        player.SetModelData(FruitModelResources.Instance.defaultData, true);
        AdaptFMD();
        co.FindCameraFOV();
        fo.FillCamera();
        foreach (SideLineObject slo in slos) slo.MoveToPlace();
    }

    public void AdaptFMD()
    {
        co.ApplyFMD(player.fmd);
        fo.ApplyFMD(player.fmd);
        foreach (SideLineObject slo in slos) slo.ApplyFMD(player.fmd);
    }

    public void StartGame()
    {
        co.StartTracking();
        fo.StartTracking();
        player.StartMoving();
    }

    [ContextMenu("Find road width")]
    public void FindRoadWidth()
    {
        Camera cam = Camera.main;
        Vector3 vp = cam.WorldToViewportPoint(player.transform.position);

        Ray r0 = cam.ViewportPointToRay(new Vector3(0, vp.y));
        Ray r1 = cam.ViewportPointToRay(new Vector3(1, vp.y));

        Vector3 v0 = r0.origin + r0.direction * (player.transform.position.y - r0.origin.y / r0.direction.y);
        Vector3 v1 = r1.origin + r1.direction * (player.transform.position.y - r1.origin.y / r1.direction.y);

        float totalWidth = Mathf.Abs(v1.x - v0.x);
        roadOuterWidth = totalWidth / 2;

        if (player.mr != null)
        {
            float size = (Matrix4x4.Scale(new Vector3(1, 0, 1)) * player.mr.bounds.size).magnitude;
            totalWidth -= size * 1.25f;
        }
        roadWidth = totalWidth / 2;
    }

    //private void Update()
    //{
    //    if (IsPlaying())
    //    {
    //        player.speed += Time.deltaTime * 0.1f;
    //    }
    //}
}
