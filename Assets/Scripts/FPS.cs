﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class FPS : MonoBehaviour
{
    public Text text;
    public float interval;

    float frames;
    float timePassed;

    private void Reset()
    {
        text = GetComponent<Text>();
    }

    void Update()
    {
        float addTime = Mathf.Min(Time.deltaTime, interval - timePassed);
        float addRatio = addTime / Time.deltaTime;
        frames += addRatio;
        timePassed += addTime;
        if (timePassed >= interval)
        {
            text.text = string.Format("{0:0.0}/{1:0.0}fps - {2:0}ms",
                frames / timePassed, Application.targetFrameRate, timePassed / frames * 1000);
            timePassed = 0;
            frames = 0;
        }
    }
}