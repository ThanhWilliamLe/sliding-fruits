﻿using UnityEngine.Advertisements;

public class AdsUtils
{
    public static bool IsShortRewardAdsAvailable()
    {
        return Advertisement.IsReady("rewardedvideoshort");
    }

    public static bool IsRewardAdsAvailable()
    {
        return Advertisement.IsReady("rewardedVideo");
    }

    public static bool IsInterstitialAdsAvailable()
    {
        return Advertisement.IsReady("interstitial");
    }

    public static bool IsBannerAdsAvailable()
    {
        return Advertisement.IsReady("banner");
    }

    public static void ShowShortRewardedAds(System.Action callback)
    {
        Advertisement.Show("rewardedvideoshort", new ShowOptions
        {
            resultCallback = (sr) =>
            {
                callback.Invoke();
            }
        });
    }

    public static void ShowRewardedAds(System.Action skip, System.Action fullWatched)
    {
        Advertisement.Show("rewardedVideo", new ShowOptions
        {
            resultCallback = (sr) =>
            {
                if (sr == ShowResult.Finished) fullWatched.Invoke();
                else skip.Invoke();
            }
        });
    }

    public static void ShowInterstitialAds()
    {
        Advertisement.Show("interstitial");
    }

    public static void ShowBannerAds()
    {
        Advertisement.Show("banner");
    }
}