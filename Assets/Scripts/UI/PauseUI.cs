﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseUI : MonoBehaviour
{
    public Text title;
    public List<Image> btnImages;

    public void ApplyFMD(FruitModelData fmd)
    {
        title.color = fmd.uiColor;
        Shadow titleShadow = title.GetComponent<Shadow>();
        if (titleShadow != null)
        {
            titleShadow.effectColor = ObjectUtils.MergeHSV(fmd.uiColor, titleShadow.effectColor, false, false, true);
        }
        foreach (Image img in btnImages)
        {
            img.color = fmd.uiColor;
        }
    }

    public void OnBackPressed()
    {
        GameManager.CurrentGame.UnpauseGame();
    }
}
