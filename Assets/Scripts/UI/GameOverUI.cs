﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverUI : MonoBehaviour
{
    public Text title;
    public Text score;
    public Text juice;
    public List<Image> btnImages;
    public Vector2 addCoinAnimSpeed = new Vector2(0.2f, 2.5f);

    public float endGameScore;
    public int endGameCoin;

    public void ApplyFMD(FruitModelData fmd)
    {
        title.color = fmd.uiColor;
        Shadow titleShadow = title.GetComponent<Shadow>();
        if (titleShadow != null)
        {
            titleShadow.effectColor =
                ObjectUtils.MergeHSV(fmd.uiColor, titleShadow.effectColor, false, false, true);
        }

        score.color = fmd.uiColor;
        Shadow scoreTextShadow = score.GetComponent<Shadow>();
        if (scoreTextShadow != null)
        {
            scoreTextShadow.effectColor =
                ObjectUtils.MergeHSV(fmd.uiColor, scoreTextShadow.effectColor, false, false, true);
        }

        foreach (Image img in btnImages)
        {
            img.color = fmd.uiColor;
        }
    }

    private void OnEnable()
    {
        StartCoroutine(AddCoinAnim(endGameScore, endGameCoin));
    }

    IEnumerator AddCoinAnim(float oriScore, int oriCoin)
    {
        float addCoinAnimTime = Mathf.Clamp(oriScore / 10, addCoinAnimSpeed.x, addCoinAnimSpeed.y);
        float time = 0;
        while (time < addCoinAnimTime)
        {
            float ratio = Mathf.Clamp01(time / addCoinAnimTime);

            score.text = Mathf.Lerp(oriScore, 0, ratio).ToString("N0");
            juice.text = Mathf.Lerp(oriCoin, oriCoin + GameManager.CurrentGame.ScoreToCoin(oriScore), ratio)
                .ToString("N0");

            yield return null;
            time += Mathf.Min(Time.deltaTime, addCoinAnimTime - time);
        }
    }

    public void OnReplayPressed()
    {
        GameManager.Instance.StartGame();
    }

    public void OnHomePressed()
    {
        GameManager.Instance.GoHome();
    }
}
