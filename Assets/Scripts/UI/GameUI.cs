﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUI : MonoBehaviour
{
    public Text scoreText;
    public List<Image> buttonImgs;

    [Header("Switches")]
    public Image dropBg;
    public Image dropFg;
    public Text dropText;
    public Text dropBoost;

    [Header("Guides")]
    public GameObject guide;
    public GameObject juiceGuide;
    public GameObject watchoutGuide;
    public GameObject tapDisabled;

    bool guideShownOnce;
    int tapCount;

    public void Tapped()
    {
        tapCount++;
        if (tapCount == 1) guide.SetActive(false);
        else if (tapCount == 3) watchoutGuide.SetActive(true);
        else if (tapCount == 7) watchoutGuide.SetActive(false);
        else if (tapCount == 9) ShowJuiceGuide();
        else if (tapCount == 13) juiceGuide.SetActive(false);
    }

    public void ApplyFMD(FruitModelData fmd)
    {
        scoreText.color = fmd.uiColor;
        Shadow scoreTextShadow = scoreText.GetComponent<Shadow>();
        if (scoreTextShadow != null)
        {
            scoreTextShadow.effectColor =
                ObjectUtils.MergeHSV(fmd.uiColor, scoreTextShadow.effectColor, false, false, true);
        }

        foreach (Image img in buttonImgs) img.color = fmd.uiColor;

        dropFg.color = fmd.trailColor;
        dropBg.color = fmd.floorColor;

        dropText.color = fmd.trailColor;
        Shadow dropTextShadow = dropText.GetComponent<Shadow>();
        if (dropTextShadow != null)
        {
            dropTextShadow.effectColor =
                ObjectUtils.MergeHSV(fmd.floorColor, dropTextShadow.effectColor, false, false, true);
        }

        dropBoost.color = fmd.trailColor;
    }

    private void Update()
    {
        scoreText.text = GameManager.CurrentGame.Score.ToString("N0");

        bool canTap = GameManager.CurrentGame.JuiceLeft >= 1 || GameManager.CurrentGame.infiniteJuiceTime > 0;
        if (tapDisabled.activeSelf && canTap) tapDisabled.SetActive(false);
        else if (!tapDisabled.activeSelf && !canTap) tapDisabled.SetActive(true);

        if (GameManager.CurrentGame.infiniteJuiceTime > 0)
        {
            dropText.text = "∞/∞";
            dropFg.fillAmount = 1;
        }
        else
        {
            float juice = GameManager.CurrentGame.JuiceLeft;
            float maxJuice = GameManager.CurrentPlayer.fmd.juiceContent.maxJuice;

            dropText.text = Mathf.FloorToInt(juice) + "/" + Mathf.FloorToInt(maxJuice);
            if (juice >= maxJuice) dropFg.fillAmount = 1;
            else dropFg.fillAmount = juice - Mathf.Floor(juice);
        }
        if (dropBoost.gameObject.activeSelf && GameManager.CurrentGame.recoverBoostTime <= 0) dropBoost.gameObject.SetActive(false);
        else if (!dropBoost.gameObject.activeSelf && GameManager.CurrentGame.recoverBoostTime > 0) dropBoost.gameObject.SetActive(true);
    }

    public void OnMenuPressed()
    {
        GameManager.CurrentGame.PauseGame();
    }

    public void ShowJuiceGuide()
    {
        if (!guideShownOnce)
        {
            juiceGuide.SetActive(true);
            guideShownOnce = true;
        }
    }

    public void OnJuicePressed()
    {
        juiceGuide.SetActive(false);
        GameManager.CurrentGame.GoStore();
    }
}
