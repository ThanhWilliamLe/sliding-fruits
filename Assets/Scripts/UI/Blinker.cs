﻿using UnityEngine;

public class Blinker : MonoBehaviour
{
    public Component target;

    [Range(0, 1)]
    public float fromAlpha = 0;
    [Range(0, 1)]
    public float toAlpha = 1;

    public float cycleInterval = 1;
    [Range(0, 1)]
    public float onTime = 0.4f;
    [Range(0, 1)]
    public float fadeTime = 0.1f;
    public bool unscaledTime;

    float ranTime;

    private void OnEnable()
    {
        if (!HasAlpha())
        {
            print("Fader failed. Disabling... " + this.name);
            enabled = false;
            return;
        }
    }

    private void Update()
    {
        float cycleLocal = (ranTime % cycleInterval) / cycleInterval;
        float t = 0;
        if (cycleLocal < fadeTime) t = cycleLocal / fadeTime;
        else if (cycleLocal < fadeTime + onTime) t = 1;
        else if (cycleLocal < fadeTime * 2 + onTime) t = 1 - (cycleLocal - onTime - fadeTime) / fadeTime;
        float alpha = Mathf.Lerp(fromAlpha, toAlpha, Mathf.Sin(t * Mathf.PI / 2f));

        SetAlpha(alpha);
        ranTime += unscaledTime ? Time.unscaledDeltaTime : Time.deltaTime;
    }

    bool HasAlpha()
    {
        return ObjectUtils.HasColor(target);
    }

    void SetAlpha(float f)
    {
        Color c = ObjectUtils.GetColor(target);
        c.a = f;
        ObjectUtils.SetColor(target, c);
    }
}