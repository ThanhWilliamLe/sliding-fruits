﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Rotator : MonoBehaviour
{
    public float speed = 360;

    void Update()
    {
        transform.Rotate(0, 0, speed * Time.deltaTime);
    }
}
