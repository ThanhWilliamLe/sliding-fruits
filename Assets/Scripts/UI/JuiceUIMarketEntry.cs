﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JuiceUIMarketEntry : MonoBehaviour
{
    public Image img;
    public Text label;
    public Text desc;
    public Text cost;
    public Button buy;
    public Button watch;
    public Image cantBuy;
    public JuiceMarketEntry jme;

    public void LoadData(JuiceMarketEntry jme, System.Action callback)
    {
        this.jme = jme;

        img.sprite = JuiceMarket.Instance.FindSpriteForEntry(jme);

        label.text = jme.label;
        desc.text = jme.desc;
        cost.text = jme.cost.ToString("N0");

        buy.gameObject.SetActive(jme.videoType == JuiceMarketEntry.VideoType.None);
        watch.gameObject.SetActive(jme.videoType != JuiceMarketEntry.VideoType.None);

        buy.onClick.RemoveAllListeners();
        buy.onClick.AddListener(new UnityEngine.Events.UnityAction(callback));

        watch.onClick.RemoveAllListeners();
        watch.onClick.AddListener(new UnityEngine.Events.UnityAction(callback));
    }

    public void ApplyFMD(FruitModelData fmd)
    {
        img.color = fmd.trailColor;
        label.color = fmd.floorColor;
        desc.color = fmd.floorColor;

        buy.targetGraphic.color = fmd.trailColor;
        watch.targetGraphic.color = fmd.trailColor;

        cantBuy.color = fmd.floorColor;
    }

    public void UpdateButtons()
    {
        buy.interactable = GameManager.Instance.Coin >= jme.cost;
        watch.interactable = jme.videoType == JuiceMarketEntry.VideoType.Long && AdsUtils.IsRewardAdsAvailable()
                            || jme.videoType == JuiceMarketEntry.VideoType.Short && AdsUtils.IsShortRewardAdsAvailable();

        if (buy.gameObject.activeSelf && !buy.interactable) cantBuy.gameObject.SetActive(true);
        else if (watch.gameObject.activeSelf && !watch.interactable) cantBuy.gameObject.SetActive(true);
        else cantBuy.gameObject.SetActive(false);
    }
}
