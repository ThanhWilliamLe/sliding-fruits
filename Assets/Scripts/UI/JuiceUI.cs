﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JuiceUI : MonoBehaviour
{
    public Text coinText;
    public Text title;
    public Image background;
    public Image backBtn;
    public Image scrollBg;

    [Header("Store")]
    public ScrollRect scroll;
    public GameObject entryPrefab;

    [Header("Got juice")]
    public Image gotJuice;
    public ParticleSystem gotJuiceParticles;
    public float gotJuiceBamTime = 0.15f;
    public float gotJuiceStayTime = 1.5f;
    public float gotJuiceScale = 5;

    List<JuiceUIMarketEntry> entries = new List<JuiceUIMarketEntry>();
    FruitModelData fmd;

    private void Awake()
    {
        PopulateMarketEntries();
    }

    private void OnEnable()
    {
        UpdateAllButtons();
        UpdateCoin();
    }

    public void ApplyFMD(FruitModelData fmd)
    {
        this.fmd = fmd;

        background.color = fmd.uiColor;
        Shadow bgShadow = background.GetComponent<Shadow>();
        if (bgShadow != null) bgShadow.effectColor =
                ObjectUtils.MergeHSV(fmd.floorColor, bgShadow.effectColor, false, false, true);

        scrollBg.color = ObjectUtils.MergeHSV(fmd.floorColor, scrollBg.color, false, false, true);
        backBtn.color = fmd.uiColor;

        title.color = fmd.uiColor;
        Shadow titleShadow = title.GetComponent<Shadow>();
        if (titleShadow != null) titleShadow.effectColor =
                ObjectUtils.MergeHSV(fmd.floorColor, titleShadow.effectColor, false, false, true);

        foreach (JuiceUIMarketEntry jume in entries)
        {
            jume.ApplyFMD(fmd);
        }
    }

    [ContextMenu("Populate")]
    public void PopulateMarketEntries()
    {
        ObjectUtils.ClearChildrens(scroll.content, true);
        entries.Clear();

        List<JuiceMarketEntry> entryResources = JuiceMarket.Instance.entries;
        foreach (JuiceMarketEntry jme in entryResources)
        {
            GameObject go = Instantiate(entryPrefab, scroll.content);
            JuiceUIMarketEntry jume = go.GetComponent<JuiceUIMarketEntry>();
            if (jume == null) DestroyImmediate(go);
            else
            {
                jume.LoadData(jme, () => OnEntryTryBuy(jume));
                if (fmd != null) jume.ApplyFMD(fmd);
                jume.UpdateButtons();
                entries.Add(jume);
            }
        }
    }

    public void UpdateAllButtons()
    {
        foreach (JuiceUIMarketEntry jume in entries)
        {
            jume.UpdateButtons();
        }
    }

    public void OnEntryTryBuy(JuiceUIMarketEntry jume)
    {
        if (jume.jme.videoType == JuiceMarketEntry.VideoType.None)
        {
            if (jume.jme.cost <= GameManager.Instance.Coin)
            {
                GameManager.CurrentGame.BoughtItem(jume.jme);
                GotJuice();
            }
        }
        else if (jume.jme.videoType == JuiceMarketEntry.VideoType.Short)
        {
            if (AdsUtils.IsShortRewardAdsAvailable())
            {
                AdsUtils.ShowShortRewardedAds(() =>
                {
                    GameManager.CurrentGame.BoughtItem(jume.jme);
                    GotJuice();
                });
            }
        }
        else if (jume.jme.videoType == JuiceMarketEntry.VideoType.Long)
        {
            if (AdsUtils.IsRewardAdsAvailable())
            {
                AdsUtils.ShowRewardedAds(() => { }, () =>
                {
                    GameManager.CurrentGame.BoughtItem(jume.jme);
                    GotJuice();
                });
            }
        }
        UpdateAllButtons();
        UpdateCoin();
    }

    public void GotJuice()
    {
        StartCoroutine(DoGotJuiceBam());
    }

    IEnumerator DoGotJuiceBam()
    {
        gotJuice.gameObject.SetActive(true);
        gotJuiceParticles.Clear(true);

        Vector3 oriScale = Vector3.one;
        float oriA = 1;
        float time = 0;
        while (time < gotJuiceBamTime)
        {
            float ratio = Mathf.Clamp01(time / gotJuiceBamTime);
            Color c = gotJuice.color;
            c.a = ratio * oriA;
            gotJuice.color = c;

            gotJuice.transform.localScale = oriScale * Mathf.Lerp(gotJuiceScale, 1, ratio);

            yield return null;
            time += Mathf.Min(Time.unscaledDeltaTime, gotJuiceBamTime - time);
        }
        Color c0 = gotJuice.color;
        c0.a = oriA;
        gotJuice.color = c0;
        gotJuice.transform.localScale = oriScale;

        gotJuiceParticles.Play();

        yield return new WaitForSecondsRealtime(gotJuiceStayTime);
        gotJuice.gameObject.SetActive(false);
    }

    public void UpdateCoin()
    {
        coinText.text = GameManager.Instance.Coin.ToString();
    }

    public void OnBackPressed()
    {
        GameManager.CurrentGame.ExitStore();
    }
}
