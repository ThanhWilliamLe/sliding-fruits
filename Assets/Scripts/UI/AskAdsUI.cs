﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AskAdsUI : MonoBehaviour
{
    public GameObject onePlus;
    public GameObject comingUp;
    public RectTransform counterValue;
    public Text counterTextValue;
    public float askTime = 5f;
    public Button skip;
    public GameObject skipBubble;
    public float skipFadeDelay;
    public float skipFadeTime;

    System.Action watchedAdsCallback;
    System.Action skipAdsCallback;

    public void Show(System.Action skipAds, System.Action watchedAds)
    {
        watchedAdsCallback = watchedAds;
        skipAdsCallback = skipAds;
        StartCoroutine(DoShow());
        StartCoroutine(DoFade());
    }

    IEnumerator DoShow()
    {
        onePlus.SetActive(true);
        comingUp.SetActive(false);
        float time = 0;
        while (time < askTime)
        {
            float ratio = Mathf.Clamp01(time / askTime);
            counterValue.anchorMin = new Vector2(ratio, counterValue.anchorMin.y);
            counterTextValue.text = (askTime - time).ToString("N0") + "s";

            yield return null;
            time += Mathf.Min(Time.deltaTime, askTime - time);
        }
        if (skipAdsCallback != null) skipAdsCallback.Invoke();
    }

    IEnumerator DoFade()
    {
        skipBubble.SetActive(false);
        skip.interactable = false;
        float oriA = skip.targetGraphic.color.a;
        float time = 0;
        while (time < skipFadeTime)
        {
            float ratio = Mathf.Clamp01(time / skipFadeTime);
            Color c = skip.targetGraphic.color;
            c.a = ratio * oriA;
            skip.targetGraphic.color = c;

            if (time == 0) yield return new WaitForSecondsRealtime(skipFadeDelay);
            yield return null;
            time += Mathf.Min(Time.deltaTime, skipFadeTime - time);
        }
        Color c0 = skip.targetGraphic.color;
        c0.a = oriA;
        skip.targetGraphic.color = c0;

        skip.interactable = true;
        skipBubble.SetActive(true);
    }

    public void OnOnePlusPressed()
    {
        if (watchedAdsCallback != null && AdsUtils.IsRewardAdsAvailable())
        {
            StopAllCoroutines();
            onePlus.SetActive(false);
            comingUp.SetActive(true);
            Invoke("AdsTime", 1f);
        }
    }

    public void AdsTime()
    {
        AdsUtils.ShowRewardedAds(skipAdsCallback, watchedAdsCallback);
    }

    public void OnSkipPressed()
    {
        StopAllCoroutines();
        if (skipAdsCallback != null) skipAdsCallback.Invoke();
    }
}
