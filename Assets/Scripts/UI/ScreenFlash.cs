﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenFlash : MonoBehaviour
{
    public Image img;
    public float flashTime;
    public bool flashAtAwake;

    private void Awake()
    {
        if (flashAtAwake) Flash(Color.white, null);
    }

    public void Flash(Color c, System.Action callback)
    {
        img.color = c;
        img.enabled = true;
        StartCoroutine(DoFade(callback));
    }

    IEnumerator DoFade(System.Action callback)
    {
        float time = 0;
        float oriAlpha = img.color.a;
        while (time <= flashTime)
        {
            float ratio = Mathf.Clamp01(time / flashTime);

            Color c = img.color;
            c.a = Mathf.Lerp(oriAlpha, 0, ratio);
            img.color = c;

            yield return null;
            time += Time.unscaledDeltaTime;
        }
        img.enabled = false;
        if (callback != null) callback.Invoke();
    }
}
