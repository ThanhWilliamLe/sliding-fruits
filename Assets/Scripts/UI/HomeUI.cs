﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HomeUI : MonoBehaviour, IPointerClickHandler
{
    [Header("Title")]
    public RectTransform upper;
    public RectTransform lower;
    public float titleAnimTime = 1f;
    public float titleLowerDelay = 0.25f;
    public float titleEase = 8;

    [Header("BG")]
    public Image bg;
    public float bgFadeTime = 0.5f;
    public Text tap;
    public Text coins;

    private void OnEnable()
    {
        upper.gameObject.SetActive(false);
        lower.gameObject.SetActive(false);
        tap.gameObject.SetActive(false);

        StartCoroutine(FadeIn(() =>
        {
            upper.gameObject.SetActive(true);
            lower.gameObject.SetActive(true);
            tap.gameObject.SetActive(true);
            StartCoroutine(Move(upper, 1, 0, null));
            StartCoroutine(Move(lower, -1, titleLowerDelay, () => tap.enabled = true));
        }));

        coins.text = GameManager.Instance.Coin.ToString("N0");
        coins.gameObject.SetActive(GameManager.Instance.Coin > 0);
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    public IEnumerator FadeIn(System.Action callback)
    {
        float time = 0;
        float a = bg.color.a;
        while (time < bgFadeTime)
        {
            float ratio = Mathf.Clamp01(time / bgFadeTime);

            Color c = bg.color;
            c.a = Mathf.Lerp(1, a, ratio);
            bg.color = c;

            yield return null;
            time += Mathf.Min(Time.deltaTime, bgFadeTime - time);
        }
        if (callback != null) callback.Invoke();
    }

    public IEnumerator Move(RectTransform rt, float fromXShift, float delay, System.Action callback)
    {
        float time = 0;
        float xMin = rt.anchorMin.x;
        float xMax = rt.anchorMax.x;
        while (time < titleAnimTime)
        {
            float ratio = 1 - Mathf.Pow(1 - Mathf.Clamp01(time / titleAnimTime), titleEase);
            rt.anchorMin = new Vector2(xMin + Mathf.Lerp(fromXShift, 0, ratio), rt.anchorMin.y);
            rt.anchorMax = new Vector2(xMax + Mathf.Lerp(fromXShift, 0, ratio), rt.anchorMax.y);

            if (time == 0) yield return new WaitForSeconds(delay);
            yield return null;
            time += Mathf.Min(Time.deltaTime, titleAnimTime - time);
        }
        if (callback != null) callback.Invoke();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        GameManager.Instance.StartGame();
    }
}
