﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Globals
{
    private static Globals instance;
    public static Globals Instance
    {
        get
        {
            if (instance == null) instance = new Globals();
            return instance;
        }
    }

    private Globals() { Init(); }

    void Init()
    {
#if UNITY_EDITOR
        Application.targetFrameRate = -1;
#else
        Application.targetFrameRate = 60;
#endif
    }
}
