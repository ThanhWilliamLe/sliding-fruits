﻿Shader "William/Lit color"
{
	Properties
	{
		_Color ("Color", Color) = (1,1,1,1)
		[Toggle]_SH ("Spherical Harmonic", Int) = 1
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" "LightMode"="ForwardBase"}
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
            #include "UnityLightingCommon.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				fixed4 diff : COLOR0;
			};

			float4 _Color;
			int _SH;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				
				half3 worldNormal = UnityObjectToWorldNormal(v.normal);
                half nl = max(0, dot(worldNormal, _WorldSpaceLightPos0.xyz));
                o.diff = nl * _LightColor0;
				if(_SH) o.diff.rgb += ShadeSH9(half4(worldNormal,1));

				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				return _Color * i.diff;
			}
			ENDCG
		}
	}
}
