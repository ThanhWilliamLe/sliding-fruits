﻿Shader "William/Lit model color"
{
	Properties
	{
		[Toggle]_SH ("Spherical Harmonic", Int) = 1
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" "Queue"="Geometry" "LightMode"="ForwardBase"}
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
            #include "UnityLightingCommon.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float4 color : COLOR;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float4 color : COLOR0;
				fixed4 diff : COLOR1;
			};

			float4 _Color;
			int _SH;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.color = v.color;
				
				half3 worldNormal = UnityObjectToWorldNormal(v.normal);
                half nl = max(0, dot(worldNormal, _WorldSpaceLightPos0.xyz));
                o.diff = nl * _LightColor0;
				if(_SH) o.diff.rgb += ShadeSH9(half4(worldNormal,1));

				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				return i.color * i.diff;
			}
			ENDCG
		}
	}
}
