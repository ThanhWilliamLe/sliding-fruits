﻿Shader "William/Lit texture"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		[Toggle]_SH ("Spherical Harmonic", Int) = 1
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" "LightMode"="ForwardBase"}
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
            #include "UnityLightingCommon.cginc"
			
			struct appdata
			{
				float4 vertex : POSITION;
				float4 normal : NORMAL;
                float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
				fixed4 diff : COLOR0;
			};
			
			sampler2D _MainTex;
			float4 _MainTex_ST;
			int _SH;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				
				half3 worldNormal = UnityObjectToWorldNormal(v.normal);
                half nl = max(0, dot(worldNormal, _WorldSpaceLightPos0.xyz));
                o.diff = nl * _LightColor0;
				if(_SH) o.diff.rgb += ShadeSH9(half4(worldNormal,1));

				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv) * i.diff;
				return col;
			}
			ENDCG
		}
	}
}
